////// 2)	FUP para calcular as raízes de uma equação do 2º grau. Classificar e mostrar as raízes calculadas. 


program exercicio2;

var 

x1, x2: real; 
valor_a, valor_b, valor_c: integer;
delta: real; 

begin

 writeln ('Digite o valor de a:  ');
    readln(valor_a);
 writeln('Digite o valor de b:  ');
    readln(valor_b);
 writeln('Digite o valor de c:  ');
    readln (valor_c);
 
 delta:= valor_b * valor_b - 4 * valor_a * valor_c;
 writeln ('O delta é ', delta);
 if delta >= 0 then 
    writeln ('o delta é positivo, então há duas raizes reais')
else 
   writeln ('o delta é negativo, então apresenta raizes imaginárias');
   
x1:= - valor_b + sqrt(delta) / 2 * valor_a;
x2:= - valor_b - sqrt(delta) / 2 * valor_a;
 
 writeln ('o valor de xi é' , x1);
 writeln ('o valor de xii é' , x2);
  
end.
