//////3)	FUP que, tendo como entrada o preço e o código de origem de um produto, mostre o preço junto com a sua procedência. 
////Caso o código não seja nenhum dos especificados, indicar que o produto é importado. Use a tabela abaixo:
////Código de origem           	     Procedência 
////     1 	                            Sul 
///Maior que 1 e menor que 5 	          Norte 
////Maior que 4 e menor que 10 	        Sudeste 
////Maior que 9 e menor que 14 	        Nordeste 
///Maior que 14 	               Outra Região/importado  



program Hello;

var

preco_a: real;
origem: integer;

begin
 
writeln('Digite o preço do produto em reais R$: ');
readln(preco_a);
writeln ('Digite o código da origem do produto: ');
readln(origem);

  if (origem = 1) then 
 writeln ('O preço do seu produto é ', preco_a:0:2,',  e vem da região sul');
  if (origem > 1) and (origem < 5) then
  writeln ('O preço do seu produto é ', preco_a:0:2,',  e vem da região norte');
  if (origem > 4) and (origem < 10) then 
 writeln ('O preço do seu produto é ', preco_a:0:2 ,',  e vem da região sudeste');
  if (origem > 9) and (origem < 14) then 
 writeln ('O preço do seu produto é ', preco_a:0:2 ,',  e vem da região nordeste');
  if (origem > 13)  then 
   writeln ('O preço do seu produto é ', preco_a:0:2, ', o produto é importado');
  
end.

